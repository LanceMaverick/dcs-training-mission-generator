
import dcs
from dcs.countries import Russia, USAFAggressors, USA

#unit convertor for nautical miles and km into meters
UNITS: dict(
    m = 1,
    nm = 1852,
    km = 1000
)

#all aircraft will be placed with these countries for red or blue
SIDES = dict(
            red = 'USAF Aggressors',
            blue = 'USA',
)


class MapPoints:
    """ Manage the map bounds and positions
        which are in cartesian coordinates (meters)
    """
    
    
    def __init__(self, map):
        """
        pass a terrain object from pydcs, e.g dcs.terrain.caucasus
        """
        self.map = map
        self.bounds = map.bounds
        self.size_x = map.bounds.right - map.bounds.left
        self.size_y = map.bounds.top - map.bounds.bottom
        self.mid_x = map.bounds.left + (self.size_x/2)
        self.mid_y = map.bounds.bottom + (self.size_y/2)
    

    def get_spawns_wp(self, n_spawns, sep_x, sep_y, units):
        """
        n_spawns: The number of head to head spawn points to generate
        sep_x: The separation between the spawn points
        sep_y: The starting (head-to-head) separation between red and blue
        units: The units of sep_x and sep_y, ("m", "km", or "nm")
        """
        sep_x = float(sep_x)*UNITS[units]
        sep_y = float(sep_y)*UNITS[units]
        y_red = self.mid_y + sep_y/2
        y_blue = self.mid_y - sep_y/2

        px = self.mid_x
        nsign = 1
        allx = []
        for s in range(n_spawns):
            allx.append(px)
            nsign = nsign*-1
            px = self.mid_x + nsign*sep_x*s

        points = []
        for x in allx:
            points.append(
                dict(
                    red = dcs.mapping.point(x, self.mid_y + sep_y),
                    blue = dcs.mapping.point(x, self.mid_y - sep_y),
                    wp = dcs.mapping.point(x, self.mid_y)
                )
            )    
        return points


class Flight:
    """
    General flight class which will be used to create
    the real flights.

    m:  The dcs.Mission() object
    plane:  String of the aircraft, e.g "F_16C_bl_50". 
            See: dcs.planes
    alt: starting altitude in meters
    loadout: loadout string (WIP) 

    """

    def __init__(self, m, plane, alt, loadout ):
        self.plane = plane
        self.alt = alt
        self.loadout = loadout
        self.mission = m


    def flight(self, side, zone, pos, wp, loadout):
        self.aircraft = getattr(dcs.planes, self.plane)
        self.start_point = dcs.Point(*pos)
        self.merge_point = dcs.Point(*wp)
        self.side = SIDES[side]


class TrainingMission:
    """
        Generates the training mission
    """
    def __init__(self, aircraft, n_spawns, map):
        """
            aircraft: list of Flight objects
            n_spawns: number of spawn points to generate
            map: dcs.terrain object, e.g dcs.terrain.caucasus
        """
        self.aircraft = aircraft
        self.n_spawns = n_spawns
        self.map = getattr(dcs.terrains, map)
        self.mission = dcs.Mission()
        self.map_points = MapPoints(self.map)
        
        self.mission.coalition['red'].add_country(USAFAggressors())


    def add_flight(self, f, side, zone, pos, wp):
        """
            Add a flight group to the mission.
            f: Flight object
            side: 'red' or 'blue'
            zone: Name of the spawn zone, e.g 'A'
            pos: Starting point. Tuple of (x,y) coordinates in m
            wp: Merge waypoint for the flight. Tuple of (x,y) coordinates in m


        """
        aircraft = getattr(dcs.planes, f.plane)
        start_point = dcs.Point(*pos)
        merge_point = dcs.Point(*wp)
        country = SIDES[side]
        name = "{}_{}_{}".format(zone, side, f.plane)
        new_flight = self.mission.flight_group_inflight(
            country,
            name,
            aircraft,
            start_point,
            f.alt,
            group_size = 4
        )
        new_flight.add_waypoint(merge_point)
        #new_flight.load_loadout() #WIP
        new_flight.set_client()

    
    def create_spawns(self):
        """
            Create all spawn points for the mission.
            Adds all flight red and blue to all spawn zones
            and adds the merge waypoint.
        """

        points = self.map.get_spawns_wp()
        for f in self.aircraft:
            for zone in range(len(points)):
                for side in SIDES.keys():
                    self.add_flight(
                        f,
                        side,
                        chr(ord('@')+(zone+1)),
                        points[zone][side],
                        points[zone]['wp']
                    )
        
                
    def generate_mission(self, filepath):
        self.create_spawns()
        self.mission.save(filepath)


